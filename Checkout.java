
package task6_oop;



/**
 *
 * @author MostafaFouad
 */
public class Checkout{   
    
  
    
    DessertItem[]  items;   
    DessertItem[]  items1;   
    int            NumberItem=0;   
    int            Tax;   
    int            TotalCost;    
   
     
             
    public  void enterItem(DessertItem item)    
    {   
        items1=items;   
        items=new DessertItem[NumberItem+1];  
        
        for(int i=0;i<NumberItem;i++)   
        {   
            items[i]=items1[i];   
        }   
        items[NumberItem++]=item;   
        items1=new DessertItem[0];   
     
    }   
   
        
    public  int numberOfItems()    
    {   
        return NumberItem;   
    }   
   
           @Override
                 
    public String toString()    
    {   
        int    LEN=DessertShoppe.MAX_ITEM_NAME_SIZE+DessertShoppe.COST_WIDTH;   
        String receipt="\n     "+DessertShoppe.STORE_NAME+"\n"+"     --------------------\n\n";   
        if(NumberItem>0)      
        {   
            int  costall=0;    
            for(int i=0;i<NumberItem;i++)   
            {   
                String Name=items[i].getName();   
                int j;   
                for(j=Name.length()-1;j>=0;j--)   
                {   
                    if(Name.charAt(j)=='\n')   
                        break;   
                }   
                int     l=Name.length()-1-j;   
                int     costitem=items[i].getCost();   
                costall+=costitem;    
                String  Scost=DessertShoppe.cents2dollarsAndCents(costitem);   
                for(int k=LEN-l-Scost.length();k>0;k--)   
                {   
                    Name+=" ";   
                }   
                Name+=Scost;   
                receipt+=Name+"\n";   
            }   
            receipt+="\n";    
            Tax=(int)(costall*DessertShoppe.TAX_RATE/100+0.5);   
            TotalCost=costall;   
            String  S=DessertShoppe.cents2dollarsAndCents((int)(Tax));   
            receipt+="Tax";   
            for(int k=LEN-"Tax".length()-S.length();k>0;k--)   
            {   
                receipt+=" ";   
            }   
            receipt+=S+"\n";   
            S=DessertShoppe.cents2dollarsAndCents((int)(TotalCost+Tax));   
            receipt+="Total Cost";   
            for(int k=LEN-"Total Cost".length()-S.length();k>0;k--)   
            {   
                receipt+=" ";   
            }   
            receipt+=S;   
            return receipt+ " EGP";   
        }   
        else   
            return "\n";   
    }   
   
              
    public int totalCost()    
    {   
        int  costall=0;    
        for(int i=0;i<NumberItem;i++)   
        {   
            int     costitem=items[i].getCost();   
            costall+=costitem;   
        }   
        return  TotalCost=costall;   
    }   
   
          
    public  int totalTax()    
    {   
        int  costall=0;    
        for(int i=0;i<NumberItem;i++)   
        {   
            int     costitem=items[i].getCost();   
            costall+=costitem;   
        }   
        Tax=(int)(costall*DessertShoppe.TAX_RATE/100+0.5);   
        return Tax;   
    }   
             
   
}   