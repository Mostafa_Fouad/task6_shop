
package task6_oop;
/**
 * 
 * @author MostafaFouad
 */
public class Sundae extends IceCream{   
    int icecreamPrice;   
    int toppingPrice;   
   
    public Sundae(String icecreamName,int icecreamCost,String toppingName,int priceTopping){   
        super(icecreamName,icecreamCost);   
        icecreamPrice=icecreamCost;   
        toppingPrice=priceTopping;   
        name="\nSundae with "+toppingName+" "+name;   
    }   
    
   @Override
   
    public int getCost()   
    {   
        return (int)(toppingPrice+icecreamPrice+3);   
    }   
}  

