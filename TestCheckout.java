
package task6_oop;
/**
 * 
 * @author MostafaFouad
 * Section 2
 * B.N. 18
 * 
 * 
 */
import java.util.Scanner;

public class TestCheckout {
static Scanner  menuItem = new Scanner(System.in);
private static String input;
private static String icecreamFlavor;
private static String sundaeFlavor;
private static String candyType;
private static int candyNumbers;
private static String cookieType;
private static int cookieNumber;
private static int choose;

    
    public static void main(String[] args) {
        
                   Checkout checkout = new Checkout();  
                 
                   menu(checkout);

    }
     static void menu(Checkout checkout){   
         
          System.out.println("Choose from menu"+"\n"+
                                       
                                        "1-Ice cream\n"+
                                        "2-Sundae\n"+
                                        "3-Candies\n"+
                                        "4-Cookies\n"+
                                        "5-Print checkout\n"
                                        
                                      );
        input=menuItem.nextLine(); 
        
         switch(input){
             
             case "1": //Ice cream
                 System.out.println("Flavor:\n"+
                                    "\n1-Vanilla\n"+
                                    "2-Chocolate\n"+
                                    "3-Caramel\n"
                         );
                 
                choose=menuItem.nextInt(); 
          switch (choose) {
              case 1:
                  icecreamFlavor="Icecream  Vanilla";
                  break;
              case 2:
                  icecreamFlavor="Icecream Chocolate";
                  break;
              case 3:
                  icecreamFlavor="Icecream Caramel";
                  
                  break;
          }
                 checkout.enterItem(new IceCream(icecreamFlavor,105));  
    
             break;
             
             case "2": //Sundae
                 
                  System.out.println("Icecream falvor:\n"+
                                    "\n1-Vanilla\n"+
                                    "2-Chocolate\n"
                         );
                   
                   choose=menuItem.nextInt(); 
                   
                    if (choose==1)
                {icecreamFlavor="\nBase vanilla";}
                else if(choose==2)
                {icecreamFlavor="\nBase Chocolate";}
                    
                     System.out.println("topping:\n"+
                                    "\n1-Caramel\n"+
                                    "2-Green Apple sauce\n"+
                                    "3-Chocolate\n"+
                                    "4-strawberry\n"
                         );
                  
                      choose=menuItem.nextInt(); 
                  
         switch (choose) {
             case 1:
                 sundaeFlavor="Caramel";
                 break;
             case 2:
                 sundaeFlavor="Green Apple sauce";
                 break;
             case 3:
                 sundaeFlavor="Chocolate";
                 break;
             case 4:
                 sundaeFlavor="strawberry";
                 break;
             default:
                 break;
         }
           checkout.enterItem(new Sundae(icecreamFlavor,145, sundaeFlavor, 50));  
 
           break;
           
             case "3": //Candy
                 
                 System.out.println("candy type:\n"+
                                    "\n1-Trident\n"+
                                    "2-Chicklets\n"
                         );
                 choose=menuItem.nextInt(); 
                 
                 switch (choose) {
             case 1:
                 candyType="Trident";
                 break;
             case 2:
                 candyType="Chicklets";
                 break;
             default:
                 break;
         }
                  System.out.print("Number of Candies:  ");
                 candyNumbers=menuItem.nextInt(); 
                 checkout.enterItem(new Candies(candyType, candyNumbers, 399));  
                 
             break;
             
             case "4": //Cookies
              System.out.println("Cookies type:\n"+
                                    "\n1-Ginger Snap\n"+
                                    "2-Dark chocolate\n"+
                                    "3-Sugar cookies\n"+
                                    "4-White chocolate\n"
                         );
              choose=menuItem.nextInt();
              
               switch (choose) {
             case 1:
                 cookieType="Ginger Snap";
                 break;
             case 2:
                 cookieType="Dark chocolate";
                 break;
             case 3:
                 cookieType="Sugar cookies";
                 break;
             case 4:
                 cookieType="White chocolate";
                 break;
             default:
                 break;
         }
                 System.out.print("Number of cookies:  ");
                 cookieNumber=menuItem.nextInt();
                 checkout.enterItem(new Cookies(cookieType, cookieNumber, 399));  
                
                 break;
                 
             case "5": //Print checkout
                 if (checkout.numberOfItems()==0){
                 System.out.println("No items have been bought yet !!\n");
                 }
                 
                 else{
                        System.out.println("\nNumber of items: " +  checkout.numberOfItems() + "\n");  
                        System.out.println("\nTotal cost:      " +  checkout.totalCost()/100 +"\n");  
                        System.out.println("\nTotal tax:       " +  checkout.totalTax() /100 + "\n");  
                        System.out.println("\nCost + Tax:      " + (checkout.totalCost()/100 + checkout.totalTax()/100+ " EGP") + "\n");  
                        System.out.println(checkout);  
                        return;
                 }
               }
  
     menu(checkout);
     
     }
     
   }

