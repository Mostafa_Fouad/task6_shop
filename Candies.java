
package task6_oop;
/**
 * @author MostafaFouad
 */

public class Candies extends DessertItem {   
    int candyNumber ;   
    int priceperpound;   
      
    public Candies(String Name,int number,int Price){   
        super(Name);      
        int   tranver=(int)((number+0.005)*100);   
        candyNumber=(int)tranver/100;   
        priceperpound=Price;   
        name=getinfo()+"\n"+name;   
    }   
    @Override
    
    public int getCost(){   
        return (int)(candyNumber*priceperpound+0.5);   
    }   
    
    public String getinfo(){   
        String info="\nCandies: ";   
        info+=candyNumber;     
        info+=" candies ";   
         
        return info;   
    }   
   
}  