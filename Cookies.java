
package task6_oop;
/**
 * 
 * @author MostafaFouad
 */
public class Cookies extends DessertItem {   
    int number;   
    int dozenPrice;   
   
    public Cookies(String Name,int Number ,int price){   
        super(Name);   
        number=Number;   
        dozenPrice=price;   
        name=getinfo()+"\n"+name;   
    }   
      @Override
    public int getCost()   
    {   
        double n=5,p=400,c=130;   
        double cost=((c*number*dozenPrice/p)/n);   
        return (int)(cost+1.75);   
    }   
    
    public String getinfo()   
    {   
        String info="\nCookies: ";   
        info+=(int)number;   
        
        return info;   
    }   
}  