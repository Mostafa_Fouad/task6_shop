
package task6_oop;

/**
 *
 * @author MostafaFouad
 */
public class IceCream extends DessertItem {
    int cost;   
   
    public IceCream(String Name,int Cost){   
        super(Name);   
        cost=Cost;   
    }   
   @Override
   
    public int getCost()   
    {   
        return (int)(cost+0.5);   
    }   
    
    
}
